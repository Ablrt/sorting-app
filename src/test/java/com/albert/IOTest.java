package com.albert;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class IOTest {

    String input;
    List<Integer> expected;

    public IOTest(String input, List<Integer> expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        return Arrays.asList(new Object[][]{
                {"1 2 4", new ArrayList<>(Arrays.asList(1, 2, 4))},
                {"1", new ArrayList<>(List.of(1))},
                {"1 2 3 4 5 6 7 8 9 10", new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))},
        });
    }

    @Test
    public void testRead() {
        Scanner sc = mock(Scanner.class);
        IO io = new IO(sc);
        when(sc.nextLine()).thenReturn(input);

        Assert.assertArrayEquals(expected.toArray(), io.read().toArray());
    }

}
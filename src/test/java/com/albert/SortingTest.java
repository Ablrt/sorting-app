package com.albert;

import junit.framework.TestCase;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

public class SortingTest {
    Sorting sorting = new Sorting();

    @Test
    public void testSort() {
        List<Integer> ls = new ArrayList<>(Arrays.asList(3,1,2));
        sorting.sort(ls);
        assertArrayEquals(new Object[]{1,2,3}, ls.toArray());
    }

    @Test
    public void testSortWithOneElement() {
        List<Integer> ls = new ArrayList<>(List.of(1));
        sorting.sort(ls);
        assertArrayEquals(new Object[]{1}, ls.toArray());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullSort() {
        sorting.sort(null);
    }

}

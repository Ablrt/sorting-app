package com.albert;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class IOExceptionTest {

    String input;

    public IOExceptionTest(String input) {
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {

        return Arrays.asList(new Object[][]{
                {""},
                {" 1"},
                {"@"},
                {"1 2 3 4 5 6 7 8 9 10 11"},
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReadExceptions() {
        Scanner sc = mock(Scanner.class);
        IO io = new IO(sc);
        when(sc.nextLine()).thenReturn(input);
        io.read();
    }

}
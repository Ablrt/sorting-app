package com.albert;

import java.util.List;
import java.util.Scanner;

public class App 
{

    public static void main( String[] args )
    {
        IO io = new IO(new Scanner(System.in));
        Sorting sorting = new Sorting();

        List<Integer> ls = io.read();
        sorting.sort(ls);
        System.out.println(ls);
    }
}

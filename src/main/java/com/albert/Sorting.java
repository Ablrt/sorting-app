package com.albert;

import java.util.Collections;
import java.util.List;

public class Sorting {
    public void sort(List<Integer> numbers) throws IllegalArgumentException {
        if (numbers == null) throw new IllegalArgumentException();
        Collections.sort(numbers);
    }
}

package com.albert;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IO {
    private Scanner sc;
    public IO(Scanner sc){
        this.sc = sc;
    }
    public List<Integer> read() throws IllegalArgumentException{
        try {
            ArrayList<Integer> numbers = new ArrayList<>(10);
            System.out.println("Input numbers:");
            String[] input = sc.nextLine().split(" ");
            if(input.length > 10) throw new IllegalArgumentException("More than 10 numbers");
            for (String s : input) {
                numbers.add(Integer.valueOf(s));
            }
            return numbers;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Input is not a number");
        }
    }
}
